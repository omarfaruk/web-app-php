-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jun 07, 2016 at 03:28 AM
-- Server version: 5.6.17
-- PHP Version: 5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `formtest`
--

-- --------------------------------------------------------

--
-- Table structure for table `form`
--

CREATE TABLE IF NOT EXISTS `form` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fname` varchar(30) DEFAULT NULL,
  `lname` varchar(30) DEFAULT NULL,
  `email` varchar(30) DEFAULT NULL,
  `pass` varchar(30) DEFAULT NULL,
  `confpass` varchar(30) DEFAULT NULL,
  `selectop` varchar(30) DEFAULT NULL,
  `sex` varchar(10) DEFAULT NULL,
  `subject` varchar(50) NOT NULL,
  `comment` varchar(1000) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=63 ;

--
-- Dumping data for table `form`
--

INSERT INTO `form` (`id`, `fname`, `lname`, `email`, `pass`, `confpass`, `selectop`, `sex`, `subject`, `comment`) VALUES
(57, 'sd', 'dd', 'faruk@gmail.com', '', '', 'TATA', 'mail', '', 'asdfasdfasdfsdafsda'),
(58, 'sd', 'dd', 'faruk@gmail.com', '', '', 'TATA', 'mail', '', 'asdfasdfasdfsdafsda'),
(59, 'sdfdsaf', 'dsafsa', 'om@gmail.com', 'dsaf', 'fdg', 'VOLVO', 'femail', '', 'etrewrtert'),
(60, 'sdfdsaf', 'dsafsa', 'om@gmail.com', '', '', 'VOLVO', 'femail', '', 'etrewrtert'),
(61, 'sdfdsaf', 'dsafsa', 'om@gmail.com', '', '', 'MOTO', 'mail', '', 'asdddddddddddddddddddddddddddddddassssssssssssssssss'),
(62, '', '', '', '', '', 'SELECT ONE', 'femail', '', '');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
