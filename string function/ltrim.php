<?php 
/*

    " " (ASCII 32 (0x20)), an ordinary space.
    "\t" (ASCII 9 (0x09)), a tab.
    "\n" (ASCII 10 (0x0A)), a new line (line feed).
    "\r" (ASCII 13 (0x0D)), a carriage return.
    "\0" (ASCII 0 (0x00)), the NUL-byte.
    "\x0B" (ASCII 11 (0x0B)), a vertical tab.
	
	
	kono variable er text k muche felte trim()function use korte hoy left theke muchte hole ltrim() abong right theke muchte hole rtrim() upore code likhle tab newline etc kaj korbe


*/



$text = "\x9\t\x9These are a few words :) ...  ";
$binary = "\x09Example string\x0A\n\n";
$hello  = "Hello \x0BWorld";
var_dump($text, $binary, $hello);

echo "<h1>Example two</h1>";
$ltim="Bangladesh is our mother land";
$trimmed =ltrim($ltim);
var_dump($trimmed);

$trimmed = ltrim($ltim, " \t.");
var_dump($trimmed);

$trimmed =ltrim($ltim, "Bang");
var_dump($trimmed);

// trim the ASCII control characters at the beginning of $binary
// (from 0 to 31 inclusive)
$binary="This is clean tag";
$clean = ltrim($binary, "\x00..\x1F");
var_dump($clean);

?>